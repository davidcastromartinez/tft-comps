Hi everyone, I'm back with a calculation of the most synergistic team composition possible. I've been asked to update my [old chart](https://i.imgur.com/XuHoyyY.png), however I actually wanted to do much more and make a compilation of charts accounting for several scenarios:

* Find the most synergistic comps with the restriction of having a certain synergy in it, like Ranger 4.

* Find the most synergistic comps allowing a spatula item.

Let's get to it:

#Overall most synergistic comps possible

* [The most synergistic teamcomps possible](https://i.imgur.com/5tH21Jh.jpg)

-

* [Extended version (Team size 4 to 8)](https://imgur.com/kGOk1GR.png)
* [Extended version (Team size 9)](https://imgur.com/PBNzcSX.png)

-

* [With 1 Spatula item](https://i.imgur.com/dlKHr2E.png)


### Ranger 4 (Kai'Sa included)

 * [Most synergistic Ranger 4 comps (Team size 8 to 9)](https://i.imgur.com/5lcZh3R.png)
 * [Most synergistic Ranger 4 comps (Team size 4 to 7)](https://i.imgur.com/8kxPlVN.png)

 * [With 1 spatula item](https://i.imgur.com/qcmmjUk.png)

### Noble 6

 * [Most synergistic Noble 6 comps](https://i.imgur.com/pLXC9Im.png)

 * [With 1 spatula item](https://i.imgur.com/JiPfBbp.png)

### Hextech 4

 * [Most synergistic Hextech 4 comps](https://i.imgur.com/JNhVIue.png)

 * [With 1 spatula item](https://i.imgur.com/J9xKOLN.png)

### Blademaster 6

 * [Most synergistic Blademaster 6 comps](https://i.imgur.com/OCKokzI.png)

 * [With 1 spatula item](https://i.imgur.com/3zaitcv.png)

### Gunslinger 6 and 4

 * [Most synergistic Gunslinger 6 comps](https://i.imgur.com/9HkcAxt.png)
 * [With 1 spatula item](https://i.imgur.com/itci1AC.png)

-

 * [Most synergistic Gunslinger 4 comps](https://i.imgur.com/9NUdpta.png)
 * [With 1 spatula item](https://i.imgur.com/CnaJXWu.png)


### Brawler 6 and 4

 * [Most synergistic Brawler 6 comps](https://i.imgur.com/rJSzyF8.png)
 * [With 1 spatula item](https://i.imgur.com/eUI3r8G.png)

-

 * [Most synergistic Brawler 4 comps (Team size 7 to 9)](https://i.imgur.com/gSDnfFr.png)
 * [Most synergistic Brawler 4 comps (Team size 4 to 6)](https://i.imgur.com/z8PrPvj.png)
 * [With 1 spatula item](https://i.imgur.com/d81ghqA.png)


### Sorcerer 9, 6 and 3

 * [Sorcerer 9 comps with FoN + spatula](https://i.imgur.com/rJSzyF8.png)

-
 * [Most synergistic Sorcerer 6 comps](https://i.imgur.com/n4oKv6S.png)
 * [With 1 spatula item](https://i.imgur.com/F5Fu3fL.png)

-
 * [Most synergistic Sorcerer 3 comps](https://i.imgur.com/twxlMFP.png)
 * [With 1 spatula item](https://i.imgur.com/9sHPYVW.png)

### Glacial 6 and 4

 * [Most synergistic Glacial 6 comps](https://i.imgur.com/0og2LP7.png)
 * [With 1 spatula item](https://i.imgur.com/1MQJkKP.png)

-

 * [Most synergistic Glacial 4 comps](https://i.imgur.com/Aj4AwgY.png)
 * [With 1 spatula item](https://i.imgur.com/h5NnHOw.png)


### Knight 6

 * [Most synergistic Knight 6 comps](https://i.imgur.com/oLUFFyw.png)

 * [With 1 spatula item](https://i.imgur.com/em0eLGx.png)


### Demon 6

 * [Most synergistic Knight 6 comps](https://i.imgur.com/MvOcmsZ.png)

 * [With 1 spatula item (Team size 6 to 7)](https://i.imgur.com/jhdKt9Y.png)

 * [With 1 spatula item (Team size 8 to 9)](https://i.imgur.com/t7shska.png)


### Shapeshifter 6 and 3

* [Most synergistic Shapeshifter 6 comps](https://i.imgur.com/zzckI1h.png)
* [With 1 spatula item](https://i.imgur.com/sxPdT7u.png)

-

* [Most synergistic Shapeshifter 6 comps](https://i.imgur.com/lfcYMKv.png)
* [With 1 spatula item](https://i.imgur.com/oEjfQMI.png)

### Wild 4

 * [Most synergistic Wild 4 comps](https://i.imgur.com/rgFFJRN.png)

 * [With 1 spatula item](https://i.imgur.com/WDaM4po.png)


### Ninja 4

 * [Most synergistic Ninja 4 comps](https://i.imgur.com/nrT5IZe.png)

 * [With 1 spatula item](https://i.imgur.com/Hl7ol1c.png)

### Void 3

 * [Most synergistic Void 3 comps](https://i.imgur.com/Oo0zr54.png)

 * [With 1 spatula item](https://i.imgur.com/xQ26X4y.png)

### Pirate 3

 * [Most synergistic Pirate 3 comps (Team size 4 to 7)](https://i.imgur.com/BOVsB5Y.png)

 * [Most synergistic Pirate 3 comps (Team size 8 to 9)](https://i.imgur.com/9Soil6d.png)

 * [With 1 spatula item (Team size 6 to 8)](https://i.imgur.com/kp3rJzi.png)

 * [With 1 spatula item (Team size 9)](https://i.imgur.com/swLCBAJ.png)

### Yordle 6

 * [Most synergistic Yordle 6 comps](https://i.imgur.com/EWvGuhC.png)

 * [With 1 spatula item](https://i.imgur.com/CnmIQ5g.png)

### Elementalist 3

 * [Most synergistic Elementalist 3 comps](https://i.imgur.com/bkxO0EF.png)
 * [With 1 spatula item (Team size 4 to 6)](https://i.imgur.com/P9KmT04.png)
 * [With 1 spatula item (Team size 7 to 9)](https://i.imgur.com/wEW6Fe1.png)


### Assassin 6

 * [Most synergistic Elementalist 3 comps](https://i.imgur.com/FLEORfk.png)
 * [With 1 spatula item (Team size 4 to 6)](https://i.imgur.com/SJvMfEf.png)
 * [With 1 spatula item (Team size 7 to 9)](https://i.imgur.com/1VbgEhQ.png)


*I might add more entries soon*

---

Some notes:

This was calculated through brute force for 3 to 6 champions teamcomps, and with aggressive pruning and heuristics for 7+. It is possible that teamcomps are missing for the 7+ range specially for 9-comps. However for 3 to 6 it should be correct and exhaustive, unless bugs happened.

These teamcomps have been generated optimizing for "number of traits active": the sum of each synergy level. This is a less strict scoring than last time, but some comps marginally superior are marked with a star.

Robot 1 and Exile 1 do not count since you can think of them as properties of the champion itself and does not help build synergies.

Cheers.
