const executeCase = require('./analyze');
const buildPng = require('../buildPng');

// const from = 4;
// const to = 9;
// const isBruteForce = true;
// const tag = "Blademaster";
// const tagLevel = 6;
// const withSpatula = true;
// executeCase(from, to, isBruteForce, tag, tagLevel, withSpatula);

const buildSet = (tag, levels) => {
  const arr = [];
  if (typeof levels === "number") levels = [levels];
  levels.forEach(level => {
    if (level > 6) {
      arr.push([4, 10, "auto", tag, level, false]);
      arr.push([4, 10, "auto", tag, level, true]);
    } else if (level > 2) {
      arr.push([4, 9, "auto", tag, level, false]);
      arr.push([4, 9, "auto", tag, level, true]);
      // arr.push([10, 10, "auto", tag, level, false]);
      // arr.push([10, 10, "auto", tag, level, true]);
    } else {
      arr.push([4, 9, "auto", tag, level, false]);
      arr.push([4, 7, "auto", tag, level, true]);
      arr.push([8, 9, "auto", tag, level, true]);
      // arr.push([10, 10, "auto", tag, level, false]);
      // arr.push([10, 10, "auto", tag, level, true]);
    }
  });
  return arr;
}

const sets = [
  // ...buildSet('Demon', [2, 4, 6]),
  // ...buildSet('Dragon', 2),
  // ...buildSet('Glacial', [2, 4, 6]),
  // ...buildSet('Hextech', [2, 4]),
  // ...buildSet('Imperial', [2, 4]),
  // ...buildSet('Noble', [3, 6]),
  // ...buildSet('Ninja', [1, 4]),
  // ...buildSet('Pirate', [3]),
  // ...buildSet('Phantom', [2]),
  // ...buildSet('Void', [3]),
  // ...buildSet('Wild', [2, 4]),
  // ...buildSet('Yordle', [3, 6]),
  // ...buildSet('Assassin', [3, 6, 9]),
  // ...buildSet('Blademaster', [3, 6, 9]),
  // ...buildSet('Brawler', [2, 4, 6]),
  // ...buildSet('Elementalist', [3]),
  // ...buildSet('Guardian', [2]),
  // ...buildSet('Gunslinger', [2, 4, 6]),
  // ...buildSet('Knight', [2, 4, 6]),
  // ...buildSet('Ranger', [2, 4]),
  // ...buildSet('Shapeshifter', [3, 6]),
  // ...buildSet('Sorcerer', [3, 6, 9]),



  // // [4, 7, "auto", null, 2, false],
  // // [4, 7, "auto", null, 2, true],
  // [8, 9, "auto", null, 2, false],
  // [8, 9, "auto", null, 2, true],
  // // [10, 10, "auto", null, 2, false],
  // // [10, 10, "auto", null, 2, true],
  // // ...buildSet('Demon', [4, 6]),
  //
  // ////
  // // ...buildSet('Dragon', 2),
  // [4, 9, "auto", "Dragon", 2, true],
  // ////
  //
  // ...buildSet('Glacial', [4, 6]),
  // ...buildSet('Hextech', [4]),
  // ...buildSet('Imperial', [4]),
  // ...buildSet('Noble', [6]),
  // ...buildSet('Ninja', [4]),
  // ...buildSet('Pirate', [3]),
  // ...buildSet('Phantom', [2]),
  // ...buildSet('Void', [3]),
  // ...buildSet('Wild', [4]),
  // ...buildSet('Yordle', [3, 6]),
  // ...buildSet('Assassin', [3, 6]),
  // ...buildSet('Blademaster', [3, 6]),
  // ...buildSet('Brawler', [4, 6]),
  // ...buildSet('Elementalist', [3]),
  // ...buildSet('Guardian', [2]),
  // ...buildSet('Gunslinger', [4, 6]),
  // ...buildSet('Knight', [4, 6]),
  // ...buildSet('Ranger', [4]),
  // ...buildSet('Shapeshifter', [3, 6]),
  // ...buildSet('Sorcerer', [3, 6, 9]),

  // [4, 8, "auto", "Pirate", 3, true],
  // [9, 9, "auto", "Pirate", 3, true],
  // [4, 6, "auto", "Assassin", 6, true],
  // [7, 9, "auto", "Assassin", 6, true],
  // [4, 6, "auto", "Elementalist", 3, true],
  // [7, 9, "auto", "Elementalist", 3, true],
  // [4, 6, "auto", "Brawler", 4, false],
  // [8, 9, "auto", "Ranger", 4, false]

  // [4, 9, "auto", "Demon", 6, false],
  // [4, 7, "auto", "Demon", 6, true],
  // [8, 9, "auto", "Demon", 6, true],

  [4, 7, "auto", "Pirate", 3, false],
  [8, 9, "auto", "Pirate", 3, false],
];

// 4 3
// 5 1
// 6 3
// 7 31
// 8 10
// 9 27


// console.log(sets);


const runSets = (async () => {
  const start = 0;
  for(let i = start; i < sets.length; ++i) {
    const parameters = sets[i];
    console.log(`Executing case ${i + 1} of ${sets.length}: ${parameters}`);
    executeCase.apply(null, parameters);
    await buildPng(__dirname + '/../datasets/results/currentResults.json', '');
  }
});

runSets();
