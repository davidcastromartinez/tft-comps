let allChampions = require('../datasets/champions.json');
const synergies = require('../datasets/synergies.json');
const allSpatulas = require('../datasets/spatulas.json')
const fs = require('fs');

const tags = { ...synergies.classes, ...synergies.origins };

delete tags.Exile;
delete tags.Robot;
// tags.Ninja.shift();

// champions = champions.slice(0, 4);

allChampions = allChampions.filter(champion => champion.name !== 'Viego');

const writeToFile = (data, fileName = 'results' + Date.now() + ' .json') => {
  // console.log(__dirname);
  // console.log(__filename);
  const filePath = __dirname + '/../datasets/results/' + fileName;
  // const filePath = './' + fileName;
  fs.writeFileSync(filePath, JSON.stringify(data, null, 2) , 'utf-8');
  console.log("Written file: " + filePath);
}

const getChampion = (name) => {
  return allChampions.find((item) => item.name === name);
}

const getMeaningfulCombos = (combos) => {
  const arr = [];
  Object.keys(combos).forEach(tag => {
    const n = combos[tag];
    const steps = tags[tag];
    if (!steps) return;
    for (let i = steps.length - 1; i >= 0; --i) {
      if (steps[i] <= n) {
        arr.push({
          tag,
          level: steps[i],
          numChampions: n,
        });
        return;
      }
    }
  });
  return arr;
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

const printTeamComp = (teamComp, numBonuses) => {
  // console.log(teamComp);
  const { team, meaningfulCombos } = teamComp;
  if (!numBonuses) numBonuses = meaningfulCombos.reduce((acc, combo) => acc + combo.numChampions, 0);
  console.log(team.join(' ') + " - " + numBonuses);
  meaningfulCombos.forEach((combo) => {
    console.log("   " + combo.tag + " " + combo.level + " ("+ combo.numChampions+")")
  });
}

const getPendingCombos = (combos) => {
  const arr = [];
  Object.keys(combos).forEach(tag => {
    const n = combos[tag];
    const steps = tags[tag];
    if (!steps) return;
    for (let i = 0; i < steps.length; ++i) {
      if (n === steps[i]) {
        return;
      }
    }
    if (n > steps[steps.length -1]) return;
    arr.push(tag);
  });
  return arr;
}

const add = (team, combos, champion) => {
  if (!team.includes(champion.name)) {
    champion.tags.forEach(tag => {
      if (!combos[tag]) combos[tag] = 0;
      combos[tag]++;
    });
  } else console.log("Warning: This should not happen.", team, " " , champion.name);
  team.push(champion.name);
}

const remove = (team, combos, champion) => {
  if (team[team.length - 1] === champion.name) team.pop();
  else {
    console.log(team, " rem ", champion.name);
    // TODO: Will not happen
    console.log("ERROR: To implement removal of non-last champion added to team. This has to modify the parameter, not make a copy.");
  }
  champion.tags.forEach(tag => {
    combos[tag]--;
    if (combos[tag] === 0) delete combos[tag];
  });
}

const calcScore = (combos) => {
  return getMeaningfulCombos(combos).reduce((acc, item) => {
    if (item.tag === 'Ninja') {
      if (item.numChampions !== 1 && item.numChampions !== 4) return acc;
    }
    return acc + item.level;
  }, 0);
}

const calcNumBonuses = (combos) => {
  return getMeaningfulCombos(combos).reduce((acc, item) => {
    if (item.tag === 'Ninja') {
      if (item.numChampions !== 1 && item.numChampions !== 4) return acc;
    }
    if (item.tag === 'Imperial') {
      if (item.level === 2) return acc + 2;
    }
    if (item.tag === 'Sorcerer' ||
        item.tag === 'Pirate' ||
        item.tag === 'Hextech' ||
        item.tag === 'Phantom' ||
        item.tag === 'Elementalist' ||
        item.tag === 'Knight' ||
        item.tag === 'Sorcerer' ||
        item.tag === 'Noble') {
      return acc + item.level;
    }
    if (item.tag === 'Wild') {
      if (item.level === 4) return acc + 4;
    }
    return acc + item.numChampions;
  }, 0);
}

const canSpatulasBeUsedInThisTeam = (team, spatulas) => {
  for (let i = 0; i < spatulas.length; ++i) {
    const spatula = spatulas[i];
    let foundAReceiver = false;
    for (let j = 0; j < team.length; ++j) {
      const championName = team[j];
      const champion = getChampion(championName);
      if (!champion.tags.includes(spatula)) {
        foundAReceiver = true;
        break;
      }
    }
    if (!foundAReceiver) return false;
  }
  return true;
}

////// RUN //////

const run = (teamSize, bruteForce = false, forceSynergy, withSpatula = false) => {
  let maxCost = [0, 1, 1, 3, 3, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5][teamSize];

  let champions = [...allChampions].filter(champion => champion.cost <= maxCost);

  let teamComps = {};
  let doneComps = {};
  let maxScore = -1;

  const considerTeamComp = (team, combos) => {
    let spatulas = [];
    // consider without adding a spatula
    const baseScore = calcScore(combos);
    // if (combos.Blademaster > 5) {
    //   console.log("I am considering a teamcomp with " + combos.Blademaster + " with score " + baseScore + " and max: " + maxScore);
    //   console.log(team);
    // }

    if (withSpatula) {
      // if force synergy then first check if you MUST put the synergy spatula
      let candidateSpatulas = allSpatulas;
      if (forceSynergy) {
        const forcedSynergy = forceSynergy[0];
        const forcedSynergyNum = forceSynergy[1];
        if (combos[forcedSynergy] < forcedSynergyNum) {
          candidateSpatulas = [];
          if (allSpatulas.includes(forcedSynergy)) candidateSpatulas.push(forcedSynergy);
          else return;
        }
      }
      candidateSpatulas.forEach(spatula => {
        if (!combos[spatula]) return;
        // !!! warning must modify back this
        combos[spatula]++;
        const score = calcScore(combos);
        // if the spatula is doing nothing then don't put this comp
        if (score >= maxScore && score > baseScore) {
          addTeamComp(team, combos, [spatula], score);
        }
        combos[spatula]--;
      });
    } else {
      if (baseScore >= maxScore) {
        addTeamComp(team, combos, [], baseScore);
      }
    }
  }

  const addTeamComp = (team, combos, spatulas, score) => {
    team = [...team];

    const doneKey = team.sort().join('') + spatulas.join('');
    if (doneComps[doneKey]) return;
    if (!canSpatulasBeUsedInThisTeam(team, spatulas)) return;

    if (score > maxScore) {
      maxScore = score;
      teamComps = { [score]: []}
      doneComps = {};
    }
    doneComps[doneKey] = true;

    const numBonuses = calcNumBonuses(combos);
    const teamComp = {
      team,
      meaningfulCombos: getMeaningfulCombos(combos),
      spatulas,
      score,
      numBonuses,
    }
    teamComps[score].push(teamComp);
  }

  const iterate = (team = [], combos = {}, start = 0) => {
    if (team.length === teamSize){
      considerTeamComp(team, combos);
      return;
    }

    if (team.length + champions.length - start < teamSize) {
      return;
    }
    const pendingCombos = getPendingCombos(combos);
    if (bruteForce || pendingCombos.length === 0) {
      for (let i = start; i < champions.length; ++i) {
        const champion = champions[i];
        // if (!team.includes(champion.name)) {
          add(team, combos, champion);
          iterate(team, combos, i + 1);
          remove(team, combos, champion);
        // }
      }
    } else {
      for (let i = start; i < champions.length; ++i) {
        const champion = champions[i];
        let progressesCombo = false;
        for (let i = 0; i < pendingCombos.length; ++i) {
          if (champion.tags.includes(pendingCombos[i])) {
            progressesCombo = true;
            break;
          }
        }
        if (progressesCombo) {
          // if (!team.includes(champion.name)) {
            add(team, combos, champion);
            iterate(team, combos, i + 1);
            remove(team, combos, champion);
          // }
        }
      }
    }
  }

  // prepare base comps for forced synergies
  const possibleBaseComps = [];
  if (forceSynergy) {
    const forcedSynergy = forceSynergy[0];
    let forcedSynergyNum = forceSynergy[1];
    const isPossibleToSpatulaForcedSynergy = allSpatulas.includes(forcedSynergy);
    if (withSpatula && isPossibleToSpatulaForcedSynergy) forcedSynergyNum--;
    let championsWithThatSynergy = champions.filter(champion =>
      champion.tags.includes(forcedSynergy)
    );

    if (forcedSynergyNum > teamSize) return null;
    if (championsWithThatSynergy.length < forcedSynergyNum) return null;

    if (championsWithThatSynergy.length === 0) {
      console.log("Error forced synergy " + forcedSynergy + " not Found.")
      return;
    }
    championsWithThatSynergy = championsWithThatSynergy.map(champion => champion.name);
    const combine = (list, start, callback) => {
      if (list.length === forcedSynergyNum) {
        callback([...list]);
        return;
      }
      for (let i = start; i < championsWithThatSynergy.length; ++i) {
        list.push(championsWithThatSynergy[i]);
        combine(list, i + 1, callback);
        list.pop();
      }
    }
    combine([], 0, (list) => {
      possibleBaseComps.push(list);
    });
  } else possibleBaseComps.push([]);

  // execute iterations
  const championsCopy = [...champions];
  const time0 = Date.now();
  possibleBaseComps.forEach((baseComp, k) => {
    champions = [...championsCopy];
    champions = champions.filter(champion =>
      !baseComp.includes(champion.name)
    );
    const baseTeam = [];
    const baseCombo = [];
    baseComp.forEach(championName => {
      add(baseTeam, baseCombo, getChampion(championName));
    });

    if (!bruteForce) {
      for (let tries = 0; tries < 32; tries++) {
        iterate(baseTeam, baseCombo);
        champions = champions.reverse();
        iterate(baseTeam, baseCombo);
        shuffle(champions);
        if (Date.now() - time0 > 500000) {
          console.log("TIMEOUT for: " + teamSize + " " + forceSynergy + " " + k);
          break;
        }
      }
    } else iterate(baseTeam, baseCombo);
  });
  console.log("Time iterating for teamSize " + teamSize + ": " + (Date.now() - time0));

  const maxSynergy = Object.keys(teamComps).reduce((acc, key) => (acc > parseInt(key, 10) ? acc : key) , 0);
  // const secondMaxSynergy = Object.keys(teamComps).reduce((acc, key) => (acc > parseInt(key, 10) ? acc : (parseInt(key, 10) < maxSynergy? key : acc)) , 0);
  //
  // console.log(teamComps);
  // console.log('maxSynergy : ' +maxSynergy );
  // console.log('secondMaxSynergy : ' +secondMaxSynergy );
  return teamComps[maxSynergy];
}


/////////////////////////////////////
// EXECUTE PARAMETERS
/////////////////////////////////////

const executeCase = (from, to, bruteForceMode, tag, tagLevel, withSpatula) => {
  const results = {};
  for (let i = from; i <= to; ++i) {
    let useBruteForce = (bruteForceMode === 'bf' ? true : (bruteForceMode === 'heur' ? false : i <= 6));
    const r = run(i, useBruteForce, (tag ? [tag, tagLevel] : null), withSpatula);
    if (r && r.length > 0) results[i] = r;
    // results[i] = run(i, true);
  }
  // console.log(JSON.stringify(results, null, 2));

  Object.keys(results).forEach(key => {
    console.log(key + " " + results[key].length);
  });
  const name = "resultsv6-" + from + "-" + to + "-" + bruteForceMode
  + (tag ? "-" + tag + tagLevel : "") + (withSpatula ? "-spat" : "");
  const data = {
    data: results,
    description: {
      name,
      tag,
      tagLevel,
      withSpatula,
    }
  }

  writeToFile(data, "currentResults.json");
  const fileName = name + ".json";
  writeToFile(data, fileName);
}

// const from = 4;
// const to = 9;
// const isBruteForce = true;
// const tag = "Blademaster";
// const tagLevel = 6;
// const withSpatula = true;
// executeCase(from, to, isBruteForce, tag, tagLevel, withSpatula);

module.exports = executeCase;
// run(5).forEach((item) => printTeamComp(item));
// run(2).forEach((item) => printTeamComp(item));

// console.log(JSON.stringify(results));
// const team = [];
// const combos = {};
// add(team, combos, getChampion("Warwick"));
// add(team, combos, getChampion("Kassadin"));
// add(team, combos, getChampion("Nidalee"));
// add(team, combos, getChampion("Ahri"));
// add(team, combos, getChampion("Shyvana"));
// add(team, combos, getChampion("Gnar"));
// add(team, combos, getChampion("AurelionSol"));
// console.log(team);
// console.log(combos);
// console.log(getMeaningfulCombos(combos));

// 2 5
// 3 16
// 4 3
// 5 2
// 6 17
