let allChampions = require('./datasets/champions.json');
const synergies = require('./datasets/synergies.json');
const fs = require('fs');

const tags = { ...synergies.classes, ...synergies.origins };

delete tags.Exile;
delete tags.Robot;
// tags.Ninja.shift();

// champions = champions.slice(0, 4);

const writeToFile = (data, fileName = 'results' + Date.now() + ' .json') => {
  const filePath = './datasets/results/' + fileName;
  fs.writeFileSync(filePath, JSON.stringify(data) , 'utf-8');
  console.log("Written file: " + filePath);
}

const getChampion = (name) => {
  return allChampions.find((item) => item.name === name);
}

const getMeaningfulCombos = (combos) => {
  const arr = [];
  Object.keys(combos).forEach(tag => {
    const n = combos[tag];
    const steps = tags[tag];
    if (!steps) return;
    for (let i = steps.length - 1; i >= 0; --i) {
      if (steps[i] <= n) {
        arr.push({
          tag,
          level: steps[i],
          numChampions: n,
        });
        return;
      }
    }
  });
  return arr;
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

const printTeamComp = (teamComp, numBenefits) => {
  // console.log(teamComp);
  const { team, meaningfulCombos } = teamComp;
  if (!numBenefits) numBenefits = meaningfulCombos.reduce((acc, combo) => acc + combo.numChampions, 0);
  console.log(team.join(' ') + " - " + numBenefits);
  meaningfulCombos.forEach((combo) => {
    console.log("   " + combo.tag + " " + combo.level + " ("+ combo.numChampions+")")
  });
}

const getPendingCombos = (combos) => {
  const arr = [];
  Object.keys(combos).forEach(tag => {
    const n = combos[tag];
    const steps = tags[tag];
    if (!steps) return;
    for (let i = 0; i < steps.length; ++i) {
      if (n === steps[i]) {
        return;
      }
    }
    if (n > steps[steps.length -1]) return;
    arr.push(tag);
  });
  return arr;
}

const add = (team, combos, champion) => {
  if (!team.includes(champion.name)) {
    champion.tags.forEach(tag => {
      if (!combos[tag]) combos[tag] = 0;
      combos[tag]++;
    });
  }
  team.push(champion.name);
}

////// RUN //////

const run = (teamSize, bruteForce = false, forceSynergy) => {
  let maxCost = [0, 1, 1, 3, 3, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5][teamSize];

  const enablePenaltyForTripleChamps = false;

  let champions = [...allChampions].filter(champion => champion.cost <= maxCost);

  let teamComps = {};

  let doneComps = {};

  let maxBenefits = -1;
  const addTeamComp = (team, combos) => {
    const meaningfulCombos = getMeaningfulCombos(combos);
    let numBenefits = meaningfulCombos.reduce((acc, item) => {
      if (item.tag === 'Ninja') {
        if (item.numChampions !== 1 && item.numChampions !== 4) return acc;
      }
      // if (item.tag === 'Imperial') {
      //   if (item.level === 2) return acc + 1;
      // }
      // if (item.tag === 'Noble') {
      //   if (item.level === 3) return acc + 1;
      //   return acc + team.length;
      // }
      // if (item.tag === 'Void') {
      //   return acc + team.length;
      // }
      // if (item.tag === 'Sorcerer') {
      //   return acc + team.length;
      // }
      // if (item.tag === 'Wild') {
      //   if (item.level === 4) return acc + team.length;
      // }

      // if (item.tag === 'Pirate') {
        //   return acc + 1;
        // }
        // if (item.tag === 'Phantom') {
          //   return acc + 1;
          // }
      return acc + item.numChampions;
    }, 0);

    if (enablePenaltyForTripleChamps) {
      const championsInfo = team.map(championName => getChampion(championName));
      let penalty = 0;
      for (let i = 0; i < championsInfo.length; ++i) {
        const champion = championsInfo[i];
        if (champion.tags.length === 3) {
          let numRelevantTags = 0;
          champion.tags.forEach(tag => {
            if (meaningfulCombos.map(mc => mc.tag).includes(tag)) numRelevantTags++;
          });
          if (numRelevantTags === 3) penalty++;
        }
      }

      numBenefits -= penalty;
    }

    if (numBenefits < maxBenefits) return;
    if (numBenefits > maxBenefits) {
      maxBenefits = numBenefits;
      teamComps = { [numBenefits]: []}
    }
    const doneKey = team.sort().join('');
    if (!doneComps[doneKey]) {
      doneComps[doneKey] = true;
    } else return;
    // if (!teamComps[numBenefits]) teamComps[numBenefits] = [];
    const teamComp = {
      team,
      meaningfulCombos,
      numBonuses: numBenefits,
    }
    teamComps[numBenefits].push(teamComp);
    // printTeamComp(teamComp);
  }

  let count = 0;
  let t0;
  let acc0 = 0;
  
  const iterate = (team = [], combos = {}, start = 0) => {
    if (team.length === teamSize){
      addTeamComp(team, combos);
      count++;
      return;
    }

    if (team.length + champions.length - start < teamSize) {
      return;
    }
    const pendingCombos = getPendingCombos(combos);
    if (bruteForce || pendingCombos.length === 0) {
      for (let i = start; i < champions.length; ++i) {
        const champion = champions[i];
        // if (!team.includes(champion.name)) {
          t0 = Date.now();
          const teamCopy = [...team];
          const combosCopy = {...combos};
          acc0 += Date.now() - t0;
          add(teamCopy, combosCopy, champion);
          iterate(teamCopy, combosCopy, i + 1);
        // }
      }
    } else {
      for (let i = start; i < champions.length; ++i) {
        const champion = champions[i];
        let progressesCombo = false;
        for (let i = 0; i < pendingCombos.length; ++i) {
          if (champion.tags.includes(pendingCombos[i])) {
            progressesCombo = true;
            break;
          }
        }
        if (progressesCombo) {
          // if (!team.includes(champion.name)) {
            const teamCopy = [...team];
            const combosCopy = {...combos};
            add(teamCopy, combosCopy, champion);
            iterate(teamCopy, combosCopy, i + 1);
          // }
        }
      }
    }
  }

  const possibleBaseComps = [];
  if (forceSynergy) {
    const forcedSynergy = forceSynergy[0];
    const forcedSynergyLevel = forceSynergy[1];
    let championsWithThatSynergy = champions.filter(champion =>
      champion.tags.includes(forcedSynergy)
    );
    if (championsWithThatSynergy.length === 0) {
      console.log("Error forced synergy " + forcedSynergy + " not Found.")
      return;
    }
    championsWithThatSynergy = championsWithThatSynergy.map(champion => champion.name);
    const combine = (list, start, callback) => {
      if (list.length === forcedSynergyLevel) {
        callback(list);
        return;
      }
      for (let i = start; i < championsWithThatSynergy.length; ++i) {
        const listCopy = [...list];
        listCopy.push(championsWithThatSynergy[i]);
        combine(listCopy, i + 1, callback);
      }
    }
    combine([], 0, (list) => {
      possibleBaseComps.push(list);
    });
  } else possibleBaseComps.push([]);

  const championsCopy = [...champions];
  possibleBaseComps.forEach(baseComp => {
    champions = [...championsCopy];
    champions = champions.filter(champion =>
      !baseComp.includes(champion.name)
    );
    const baseTeam = [];
    const baseCombo = [];
    baseComp.forEach(championName => {
      add(baseTeam, baseCombo, getChampion(championName));
    });

    const time0 = Date.now();
    if (!bruteForce) {
      for (let tries = 0; tries < 5; tries++) {
        iterate(baseTeam, baseCombo);
        champions = champions.reverse();
        iterate(baseTeam, baseCombo);
        shuffle(champions);
      }
    } else iterate(baseTeam, baseCombo);
    console.log("Time iterating: " + (Date.now() - time0));
    console.log("Time acc0 ", acc0);
  })

  const maxSynergy = Object.keys(teamComps).reduce((acc, key) => (acc > parseInt(key, 10) ? acc : key) , 0);
  // const secondMaxSynergy = Object.keys(teamComps).reduce((acc, key) => (acc > parseInt(key, 10) ? acc : (parseInt(key, 10) < maxSynergy? key : acc)) , 0);
  //
  // console.log(teamComps);
  // console.log('maxSynergy : ' +maxSynergy );
  // console.log('secondMaxSynergy : ' +secondMaxSynergy );
  return teamComps[maxSynergy];
}

const results = {};
for (let i = 5; i < 6; ++i) {
  results[i] = run(i, true);
}
// console.log(JSON.stringify(results, null, 2));

Object.keys(results).forEach(key => {
  console.log(key + " " + results[key].length);
});
writeToFile(results, "resultsTest.json");


// run(5).forEach((item) => printTeamComp(item));
// run(2).forEach((item) => printTeamComp(item));

// console.log(JSON.stringify(results));
// const team = [];
// const combos = {};
// add(team, combos, getChampion("Warwick"));
// add(team, combos, getChampion("Kassadin"));
// add(team, combos, getChampion("Nidalee"));
// add(team, combos, getChampion("Ahri"));
// add(team, combos, getChampion("Shyvana"));
// add(team, combos, getChampion("Gnar"));
// add(team, combos, getChampion("AurelionSol"));
// console.log(team);
// console.log(combos);
// console.log(getMeaningfulCombos(combos));

// 2 5
// 3 16
// 4 3
// 5 2
// 6 17
