let allChampions = require('./datasets/champions.json');
const synergies = require('./datasets/synergies.json');
const fs = require('fs');

const tags = { ...synergies.classes, ...synergies.origins };

delete tags.Exile;
delete tags.Robot;
// tags.Ninja.shift();

// champions = champions.slice(0, 4);

const getChampion = (name) => {
  return champions.find((item) => item.name === name);
}

const getMeaningfulCombos = (combos) => {
  const arr = [];
  Object.keys(combos).forEach(tag => {
    const n = combos[tag];
    const steps = tags[tag];
    if (!steps) return;
    for (let i = steps.length - 1; i >= 0; --i) {
      if (steps[i] <= n) {
        arr.push({
          tag,
          level: steps[i],
          numChampions: n,
        });
        return;
      }
    }
  });
  return arr;
}

const printTeamComp = (teamComp, numBenefits) => {
  // console.log(teamComp);
  const { team, meaningfulCombos } = teamComp;
  if (!numBenefits) numBenefits = meaningfulCombos.reduce((acc, combo) => acc + combo.numChampions, 0);
  console.log(team.join(' ') + " - " + numBenefits);
  meaningfulCombos.forEach((combo) => {
    console.log("   " + combo.tag + " " + combo.level + " ("+ combo.numChampions+")")
  });
}

const getPendingCombos = (combos) => {
  const arr = [];
  Object.keys(combos).forEach(tag => {
    const n = combos[tag];
    const steps = tags[tag];
    if (!steps) return;
    for (let i = 0; i < steps.length; ++i) {
      if (n === steps[i]) {
        return;
      }
    }
    if (n > steps[steps.length -1]) return;
    arr.push(tag);
  });
  return arr;
}

const add = (team, combos, champion) => {
  if (!team.includes(champion.name)) {
    champion.tags.forEach(tag => {
      if (!combos[tag]) combos[tag] = 0;
      combos[tag]++;
    });
  }
  team.push(champion.name);
}

const run = (teamSize) => {
  const maxCost = [0, 1, 1, 2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5][teamSize];
  const champions = [...allChampions].filter(champion => champion.cost <= maxCost);

  let teamComps = {};

  let maxBenefits = -1;
  const addTeamComp = (team, combos) => {
    const meaningfulCombos = getMeaningfulCombos(combos);
    const numBenefits = meaningfulCombos.reduce((acc, item) => {
      if (item.tag === 'Ninja') {
        if (item.numChampions !== 1 && item.numChampions !== 4) return acc;
      }
      // if (item.tag === 'Imperial') {
      //   if (item.level === 2) return acc + 1;
      // }
      // if (item.tag === 'Noble') {
      //   if (item.level === 3) return acc + 1;
      //   return acc + team.length;
      // }
      // if (item.tag === 'Void') {
      //   return acc + team.length;
      // }
      // if (item.tag === 'Sorcerer') {
      //   return acc + team.length;
      // }
      // if (item.tag === 'Wild') {
      //   if (item.level === 4) return acc + team.length;
      // }

      // if (item.tag === 'Pirate') {
        //   return acc + 1;
        // }
        // if (item.tag === 'Phantom') {
          //   return acc + 1;
          // }
      return acc + item.numChampions;
    }, 0);
    const teamComp = {
      team,
      meaningfulCombos,
      numBonuses: numBenefits,
    }

    if (numBenefits < maxBenefits) return;
    if (numBenefits > maxBenefits) {
      maxBenefits = numBenefits;
      teamComps = { [numBenefits]: []}
    }
    // if (!teamComps[numBenefits]) teamComps[numBenefits] = [];
    teamComps[numBenefits].push(teamComp);
    // printTeamComp(teamComp);
  }

  let count = 0;
  const iterate = (team = [], combos = {}, start = 0) => {
    if (team.length === teamSize){
      addTeamComp(team, combos);
      count++;
      return;
    }
    // if (start === champions.length) {
    //   addTeamComp(team, combos);
    //   count++;
    //   return;
    // }
    if (team.length + champions.length - start < teamSize) {
      return;
    }
    const pendingCombos = getPendingCombos(combos);
    // console.log(team);
    // console.log(combos);
    // console.log(pendingCombos);
    if (pendingCombos.length === 0) {
    // if (true) {
      for (let i = start; i < champions.length; ++i) {
        const champion = champions[i];
        // if (!team.includes(champion.name)) {
          const teamCopy = [...team];
          const combosCopy = {...combos};
          add(teamCopy, combosCopy, champion);
          iterate(teamCopy, combosCopy, i + 1);
        // }
      }
    } else {
      for (let i = start; i < champions.length; ++i) {
        const champion = champions[i];
        let progressesCombo = false;
        for (let i = 0; i < pendingCombos.length; ++i) {
          if (champion.tags.includes(pendingCombos[i])) {
            progressesCombo = true;
            break;
          }
        }
        if (progressesCombo) {
          // if (!team.includes(champion.name)) {
            const teamCopy = [...team];
            const combosCopy = {...combos};
            add(teamCopy, combosCopy, champion);
            iterate(teamCopy, combosCopy, start + 1);
          // }
        }
      }
    }
  }

  const iterate2 = (team = [], combos = {}, tagsStart = {}, start = 0) => {
    if (team.length === teamSize){
      addTeamComp(team, combos);
      count++;
      return;
    }
    // if (start === champions.length) {
    //   addTeamComp(team, combos);
    //   count++;
    //   return;
    // }
    // if (team.length + champions.length - start < teamSize) {
    //   return;
    // }
    const pendingCombos = getPendingCombos(combos);
    // console.log(team);
    // console.log(combos);
    // console.log(pendingCombos);
    if (pendingCombos.length === 0) {
    // if (true) {
      for (let i = start; i < champions.length; ++i) {
        const champion = champions[i];
        if (!team.includes(champion.name)) {
          const teamCopy = [...team];
          const combosCopy = {...combos};
          add(teamCopy, combosCopy, champion);
          iterate2(teamCopy, combosCopy, tagsStart, i + 1);
        }
      }
    } else {
      pendingCombos.forEach(pendingCombo => {
        let tagStart = tagsStart[pendingCombo];
        for (let i = tagStart; i < champions.length; ++i) {
          const champion = champions[i];
          if (champion.tags.includes(pendingCombo)) {
            if (!team.includes(champion.name)) {
              let isValid = true;
              for (let j = 0; j < champion.tags.length; ++j) {
                const tag = champion.tags[j];
                if (tagsStart[tag] > i) {
                  isValid = false;
                  break;
                }
              }
              if (!isValid) continue;
              const teamCopy = [...team];
              const combosCopy = {...combos};
              const tagsStartCopy = {...tagsStart};
              tagsStartCopy[pendingCombo] = i + 1;
              add(teamCopy, combosCopy, champion);
              iterate2(teamCopy, combosCopy, tagsStartCopy, start);
            }
          }
        }
      });
    }
  }


  const getPendingCombosToCompleteSelection = (combos, selectedCombos) => {
    const arr = [];
    selectedCombos.forEach((selectedCombo) => {
      const tag = selectedCombo[0];
      const level = selectedCombo[1];
      if (!combos[tag] || combos[tag] < level) {
        if (!arr.includes(tag)) arr.push(tag);
      }
    });
    return arr;
  }


  let possibleCombos = [];

  const iterate3 = (team = [], combos = {}, selectedCombos= [], iStart = 0, start = 0) => {
    if (team.length === teamSize){
      addTeamComp(team, combos);
      count++;
      return;
    }
    if (selectedCombos.length < 2) {
      for (let i = iStart; i < possibleCombos.length; ++i) {
        iterate3(team, combos, [...selectedCombos, possibleCombos[i]], i + 1)
      }
      return;
    }
    if (start === champions.length) {
      // addTeamComp(team, combos);
      // count++;
      return;
    }
    // if (team.length + champions.length - start < teamSize) {
    //   return;
    // }

    const pendingCombos = getPendingCombosToCompleteSelection(combos, selectedCombos);

    // const pendingCombos = getPendingCombos(combos);
    // console.log(team);
    // console.log(combos);
    // console.log(pendingCombos);
    if (pendingCombos.length === 0) {
    // if (true) {
      for (let i = 0; i < champions.length; ++i) {
        const champion = champions[i];
        // if (!team.includes(champion.name)) {
        if (!champion.tags.includes(selectedCombos[0][0]) ||
            !champion.tags.includes(selectedCombos[1][0])
        ) {
          const teamCopy = [...team];
          const combosCopy = {...combos};
          add(teamCopy, combosCopy, champion);
          iterate3(teamCopy, combosCopy, selectedCombos, iStart, start);
        }
      }
    } else {
      for (let i = start; i < champions.length; ++i) {
        const champion = champions[i];
        let isValid = false;
        for (let j = 0; j < pendingCombos.length; ++j) {
          const pendingTag = pendingCombos[j];
          if (champion.tags.includes(pendingTag)) {
            isValid = true;
            break;
          }
        }
        if (isValid
          // && !team.includes(champion.name)
        ) {
          const teamCopy = [...team];
          const combosCopy = {...combos};
          add(teamCopy, combosCopy, champion);
          iterate3(teamCopy, combosCopy, selectedCombos, iStart, i + 1);
        }
      }

      }
    }




  // const initialTagsStart = {};
  // for (let key in tags) {
  //   initialTagsStart[key] = 0;
  // }
  // iterate2([], {}, initialTagsStart);

  // for (let key in tags) {
  //   const levels = tags[key];
  //   levels.forEach(level => {
  //     possibleCombos.push([key, level]);
  //   });
  // }
  // iterate3();

  iterate([], {});

  const maxSynergy = Object.keys(teamComps).reduce((acc, key) => (acc > parseInt(key, 10) ? acc : key) , 0);
  // const secondMaxSynergy = Object.keys(teamComps).reduce((acc, key) => (acc > parseInt(key, 10) ? acc : (parseInt(key, 10) < maxSynergy? key : acc)) , 0);

  return teamComps[maxSynergy];
}

const results = {};
for (let i = 2; i < 6; ++i) {
  results[i] = run(i);
}

Object.keys(results).forEach(key => {
  console.log(key + " " + results[key].length);
});

// run(7).forEach((item) => printTeamComp(item));
// console.log(JSON.stringify(results));
fs.writeFileSync('./datasets/results301.json', JSON.stringify(results) , 'utf-8');


// const team = [];
// const combos = {};
// add(team, combos, getChampion("Warwick"));
// add(team, combos, getChampion("Kassadin"));
// add(team, combos, getChampion("Nidalee"));
// add(team, combos, getChampion("Ahri"));
// add(team, combos, getChampion("Shyvana"));
// add(team, combos, getChampion("Gnar"));
// add(team, combos, getChampion("AurelionSol"));
// console.log(team);
// console.log(combos);
// console.log(getMeaningfulCombos(combos));

// 2 5
// 3 16
// 4 3
// 5 2
// 6 17
