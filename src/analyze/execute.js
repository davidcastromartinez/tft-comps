const executeCase = require('./analyze');
//const buildPng = require('../buildPng');

// from, to, bruteForceMode, tag, tagLevel, withSpatula
executeCase(4, 10, "heur", null, 0, false);
// buildPng(__dirname + '/../datasets/results/currentResults.json')

/*
const runSets = (async () => {
    const start = 0;
    for (let i = start; i < sets.length; ++i) {
        const parameters = sets[i];
        console.log(`Executing case ${i + 1} of ${sets.length}: ${parameters}`);
        executeCase.apply(null, parameters);
        await buildPng(__dirname + '/../datasets/results/currentResults.json', '');
    }
});

runSets();
*/
