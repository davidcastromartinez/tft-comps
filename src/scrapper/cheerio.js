// Write Javascript code here
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');


const URL = "https://blitz.gg/tft/champions/overview?set=5_5";

const requestBody = (url, cb) => {
  request(url, (err, res, body) => {
  	if (err) console.log(err);
    else cb(body);
  });
}


requestBody(URL, (body) => {
  let $ = cheerio.load(body);
  const champions = $('.table-content').children().map(function (index) {
    const name = $(this).find('.ChampRow__ChampName-sc-1s3nv6z-3').text();
    let cost = $(this).find('.ChampRow__CostColumn-sc-1s3nv6z-5').text(); // match-gold3
    cost = cost.substring(cost.length - 1);
    cost = parseInt(cost, 10);
    const tags = $(this).find('.gWhUEk').map((index, item) => {
        // console.log("EACH: " + $(item).attr("name"));
        return $(item).attr("name");
      }).toArray();

    // console.log("VVVV");
    // console.log(tags.length);
    // console.log(tags);
    return { name, cost, tags }
  }).toArray();
  // console.log(champions);
  const championsJSON = JSON.stringify(champions);
  console.log(championsJSON);
});

// requestBody(URL, (body) => {
// 		let $ = cheerio.load(body);
// 		const champions = $('.ChampRow__TableRow-sc-1s3nv6z-0').map(function(index) {
//       const name = $(this).find('.ChampRow__ChampName-sc-1s3nv6z-5').text();
//       let cost = $(this).find('.ChampRow__CostColumn-sc-1s3nv6z-8').text();
//       cost = parseInt(cost, 10);
//       const tags = $(this).find('.ChampRow__TypesColumn-sc-1s3nv6z-6').children()
//       .map((index, item) => {
//         // console.log("EACH: " + $(item).attr("name"));
//         return $(item).attr("name");
//       }).toArray();

//       // console.log("VVVV");
//       // console.log(tags.length);
//       // console.log(tags);
//       return { name, cost, tags }
// 		}).toArray();
//     // console.log(champions);
//     const championsJSON = JSON.stringify(champions);
//     console.log(championsJSON);
// });


