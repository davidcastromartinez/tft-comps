let allChampions = require('../datasets/champions.json');
const synergies = require('../datasets/synergies.json');

const traits = [...Object.keys(synergies.origins), ... Object.keys(synergies.classes)];

// console.log(traits);
const found = {};
allChampions.forEach(champion => {
  champion.tags.forEach(tag => {
    if (!traits.includes(tag)) console.error("Error: Champion " + champion.name + " has tag " + tag + " that is not found in synergies.json");
    found[tag] = true;

  })
});

if (Object.keys(found).length !== traits.length) {
  console.error("Error: Number of unique champion tags is different from num. of synergies");
  console.error(found);
  console.error(traits);
}
