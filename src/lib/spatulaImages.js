export default {
  Academy: require('../assets/spatulas/AcademyEmblem.png'),
  Arcanist: require('../assets/spatulas/ArcanistEmblem.png'),
  Imperial: require('../assets/spatulas/ImperialEmblem.png'),
  Mutant: require('../assets/spatulas/MutantEmblem.png'),
  Chemtech: require('../assets/spatulas/ChemtechEmblem.png'),
  Challenger: require('../assets/spatulas/ChallengerEmblem.png'),
  Assassin: require('../assets/spatulas/AssassinEmblem.png'),
  Syndicate: require('../assets/spatulas/SyndicateEmblem.png'),
  "Force of Nature": require('../assets/spatulas/TacticiansCrown.png'),
};
