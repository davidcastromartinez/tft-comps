import champions from '../datasets/champions.json';

const getChampionInfo = (championName) => {
  return champions.find(item => item.name === championName);
}

export default getChampionInfo;
