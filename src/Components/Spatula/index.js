import React from 'react';
import './styles.css';
import spatulaImages from '../../lib/spatulaImages';

// const mapSpatulaTypeToName = (type) => {
//   const names = {
//     Assassin: 'Youmuu\'s Ghostblade',
//     Knight: 'Knight\'s Vow',
//     Glacial: 'Frozen Mallet',
//     Sorcerer: 'Yuumi',
//     Blademaster: 'Blade of the Ruined King',
//     Demon: 'Darkin',
//   };
//   if (!names[type]) return type;
//   return names[type];
// }

function Spatula(props) {
  const { type } = props;
  // const name = mapSpatulaTypeToName(type);
  return (
    <div className={props.className + " baseSpatula"}>
      <div className="wrapper">
        <img
          className="spatulaImage"
          src={spatulaImages[type]}
          alt={type}
        />
      </div>
    </div>
  );
  // <div className="name" style={{color: "white"}}>{}</div>
}

export default Spatula;
