import React from 'react';
import './styles.css';
import synergyImages from '../../lib/synergyImages';
import tagToColor from '../../lib/tagToColor';

function Badge(props) {
  const { tag } = props;
  const src = synergyImages[tag];
  const style = { backgroundColor: tagToColor[tag] };

  return (
    <img className="badge"
    style={style}
     alt={tag}
     src={src}
     />
  );
}

export default Badge;
