import React from 'react';
import Badge from '../Badge';
import './styles.css';
import championImages from '../../lib/championImages';
import getChampionInfo from '../../lib/getChampionInfo';

const mapCostToBorderColor = (cost)=> {
  const arr = ['#d4d4d4', '#139e09', '#335be3', '#9d399d', '#d1d438'];
  return arr[cost - 1];
}

function Champion(props) {
  const { name } = props;
  const championInfo = getChampionInfo(name);
  const borderColor = mapCostToBorderColor(championInfo.cost);
  const borderStyle = { border : "1px solid " + borderColor };
  const badges = championInfo.tags.map(tag => {
    return <Badge tag={tag} key={tag} />
  });
  return (
    <div className="base">
      <div className="champion">
          <div className="badgesContainer">{badges}</div>
          <img
            className="championImage"
            style={borderStyle}
            src={championImages[name]}
            alt={name}
          />
      </div>
      <div className="name" style={{color: borderColor}}>{name}</div>
    </div>
  );
}

export default Champion;
