import React, { Component } from 'react';
// import logo from './logo.svg';
import './styles.css';
import Section from '../Section';
import synergyImages from '../../lib/synergyImages';
import tagToColor from '../../lib/tagToColor';
import starSrc from '../../assets/star.png';

const spatulaSrc = require('../../assets/spatulas/Spatula.png')

const jsonData = require('../../datasets/results/currentResults.json');

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

const patterns = [
  "diagmonds",
  "arabesque",
  "argyle",
  "blizzard",
  "black-twill",
  "dark-brick-wall",
  "dark-fish-skin",
  "dark-mosaic",
  "dark-wood",
  "cartographer",
  "inspiration-geometry",
  "lined-paper-2",
  "random-grey-variations",
  "xv",
  "stardust",
  "stressed-linen",
  "batthern",
  "gplay",
  "snow",
  "wavecut",
  "white-diamond-dark",
  "black-lozenge",
  "squares",
];

// const description1 = "This list shows the most synergistic team compositions possible by maximizing the number of active traits. 3 champions with 2 active traits each is scores as 6.";
// const description2 = "Traits that affect only 1 member or the whole team, are still counted once per champion with the trait.";
// // const description2 = "Traits that affect only 1 member or the whole team, are counted accordingly.";
// const description3 = "Higher tier champions appear only on team sizes that correspond to player levels that would allow them to appear.";
// const description4 = "This list is not guaranteed to be exhaustive for 7+ size teamcomps.";
// const renderDescriptions = (description1, description2, description3, description4) => (
//   <div>
//     <p className="description">
//       {description1}
//     </p>
//     <p className="description">
//       {description2}
//     </p>
//     <p className="description">
//       {description3}
//     </p>
//     <p className="description">
//       {description4}
//     </p>
//   </div>
// );


const renderTitle = (title, tag = null, num = -1, withSpatula = false) => {
  const src = synergyImages[tag];
  let spatula = null;
  if (withSpatula) {
    spatula = (
      <div className="spatulaWrapper">
        <div className="fixedSpace" />
        <div className="fixedSpace" />
        <div className="fixedSpace" />
        <div>
         {" with a "}
        </div>
        <div className="fixedSpace" />
        <div className="fixedSpace" />
      <img src={spatulaSrc} className="spatulaImage2" alt="spatulaTitle" />
      </div>

    );
  }
  const color = tagToColor[tag];
  let tagComp = null;
  if (tag) {
    tagComp = (
      <div className="title2" style={{color}}>
        <img className="tag"
          alt={tag}
          src={src}
        />
        <div className="fixedSpace" />
        <div className="fixedSpace" />
        <div className="fixedSpace" />
        {tag + ' ' + num}
        <div className="fixedSpace" />
        <div className="fixedSpace" />
        <div className="fixedSpace" />
        <img className="tag"
        alt={tag}
        src={src}
        />
      </div>
    );
  }
  return (
    <div>
      <p className="title1">
        {(Array.isArray(title) ? title.map((s, index) => <React.Fragment key={s}>{s}
        {(index < title.length - 1 ?<br/>:null)}
        </React.Fragment>): title)}
      </p>
      <div className="title2" style={{color}}>
        {tagComp}
        {spatula}
      </div>
    </div>
  );

}

const choosePattern = () => {
  return patterns[getRandomInt(0, patterns.length)];
}

const chooseColor = () => { // 0 84 148
  // #005494
  const r = parseInt(17 * (1.0 + getRandomArbitrary(-0.2, 0.2)), 10);
  const g = parseInt(26 * (1.0 + getRandomArbitrary(-0.2, 0.2)), 10);
  const b = parseInt(53 * (1.0 + getRandomArbitrary(-0.2, 0.2)), 10);
  return `rgb(${r}, ${g}, ${b})`;
}

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shouldShowFooter: false,
    };
    this.shouldShowFooter = this.shouldShowFooter.bind(this);
  }

  shouldShowFooter(isVisible) {
    if (!this.state.shouldShowFooter) {
      this.setState({
        shouldShowFooter: true,
      });
    }
  }

  render() {
    const items = [];
    const { data, description } = jsonData;
    const keys = Object.keys(data).map(k => parseInt(k, 10)).sort((a, b) => a - b).reverse();
    keys.forEach(key => {
      if (data[key][0].team.length < 3) return;
      // if (data[key][0].team.length > 8) return;
      items.push(<Section
        key={key}
        data={data[key]}
        shouldShowFooter={this.shouldShowFooter}
     />);
    });
    const title1 = "The most synergistic";
    const title2 = "teamcomps";
    const title = [title1, title2, "~ Set 6 ~"]

    const pattern = choosePattern();
    const dynamicBackground = {
      backgroundColor: chooseColor(),
      backgroundImage: 'url(https://www.transparenttextures.com/patterns/' + pattern + '.png)',
    }
    /*
    let footer = null;
    if (this.state.shouldShowFooter) {
      const starText = "Teamcomps with a star are slightly more synergistic than the others because they have something like Demon 3, where the 3rd Demon does not help towards synergies but benefits from them."
      footer = (
        <div className="appFooter">
          <img
            className="footerStar"
            src={starSrc}
            alt="star"
          />
          {starText}
        </div>
      );
    }
    */
    return (
      <div className="App" style={{...dynamicBackground}}>
        <div className="header">
          <img className="logo" alt="logo" src={require('../../assets/tft-logo.png')} />
          {renderTitle(title, description.tag, description.tagLevel, description.withSpatula)}
        </div>
        {items}
        {/* footer */}
      </div>
    );
  }
}

export default Main;
