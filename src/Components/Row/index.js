import React from 'react';
import './styles.css';
import Champion from '../Champion';
import Spatula from '../Spatula';
import tagToColor from '../../lib/tagToColor';
import getChampionInfo from '../../lib/getChampionInfo';
import starSrc from '../../assets/star.png';

function Row(props) {
  const { teamComp } = props;
  const sortedTeam = teamComp.team.slice(0)
  .sort((a, b) => {
    return getChampionInfo(a).cost - getChampionInfo(b).cost;
  });
  const champions = sortedTeam.map(championName =>
    <Champion key={championName} name={championName} />
  );
  const combosInfo = teamComp.meaningfulCombos.map(combo => {
    return (
      <div key={combo.tag} className="combo" style={{ color: tagToColor[combo.tag] }}>
        {combo.tag} {combo.level}
      </div>
    )
  });
  let rowClassName = "row";
  let combosContainerClassName = "combosContainer";
  if (combosInfo.length > 8) {
    combosContainerClassName += " longCombosContainer";
    rowClassName += " longRow";
  }

  /*
  for (let i = 10; i <= teamComp.team.length; ++i) {
    champions.unshift(<Spatula className="floatingSpatula" key={"Force of Nature " + i} type={"Force of Nature"} />)
  }
  */

  if (teamComp.spatulas && teamComp.spatulas.length > 0) {
    teamComp.spatulas.forEach(spatula => {
      champions.unshift(<Spatula className="floatingSpatula" key={spatula} type={spatula} />)
    });
  }
  let star = null;
  if (teamComp.isMarginallyBetter) {
    star = (<div key={"marginallyBetter"}>
    <img
      className="star"
      src={starSrc}
      alt="star"
    />
     </div>);
  }
  return (
    <div className={rowClassName}>
      {star}
      {champions}
      <div className={combosContainerClassName}>
        {combosInfo}
      </div>
    </div>
  );
}

export default Row;
