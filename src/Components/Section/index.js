import React from 'react';
// import logo from './logo.svg';
import './styles.css';
import Row from '../Row';
import getChampionInfo from '../../lib/getChampionInfo';

const calculateTotalCostOfTeamComp = (teamComp) => {
  return teamComp.team.map(championName => getChampionInfo(championName))
    .map(championInfo => championInfo.cost).reduce((acc, v) => acc + v, 0);
}

let hack = true;

function Section(props) {
  const { data } = props;
  const rows = [];
  let teamComps = [...data];
  const originaLength = teamComps.length;
  const teamSize = teamComps[0].team.length;


  // if (teamSize >= 9) return null;
  const isAggressiveFilteringEnabled = false;


  teamComps = teamComps.map(teamComp => {
    const comp = {...teamComp};
    const totalCost = calculateTotalCostOfTeamComp(comp);
    comp.totalCost = totalCost;
    return comp;
  });
  const maxNumBonuses = teamComps.reduce((max, teamComp) => (teamComp.numBonuses > max ? teamComp.numBonuses : max ), -1);
  const minNumBonuses = teamComps.reduce((min, teamComp) => (teamComp.numBonuses < min ? teamComp.numBonuses : min ), 9999);
  // console.log(maxNumBonuses);
  // console.log(minNumBonuses);
  const isThereMarginallyBetterComps = (maxNumBonuses > minNumBonuses);
  let numMarginallyBetter = 0;
  teamComps = teamComps.map(teamComp => {
    const comp = {...teamComp};
    comp.isMarginallyBetter = (isThereMarginallyBetterComps && comp.numBonuses === maxNumBonuses);
    if (comp.isMarginallyBetter) numMarginallyBetter++
    return comp;
  });
  let wasFilterApplied = false;

  if (isAggressiveFilteringEnabled && teamComps.length > 30 && numMarginallyBetter >= 1) {
    console.log("WARNING: aggressive filtering");
    teamComps = teamComps.filter(teamComp => teamComp.isMarginallyBetter);
    teamComps.forEach(teamComp => { teamComp.isMarginallyBetter = false });
    wasFilterApplied = true;
  }
  if (isThereMarginallyBetterComps && hack && !wasFilterApplied)  {
    hack = true;
    props.shouldShowFooter(true);
  }
  // if (teamComps[0].spatulas && teamComps[0].spatulas.length > 0) {
    teamComps = teamComps.sort((a, b) => {
      if (a.isMarginallyBetter && !b.isMarginallyBetter) return -1;
      if (b.isMarginallyBetter && !a.isMarginallyBetter) return 1;
      let sa = a.spatulas;
      if (sa) sa = sa[0];
      let sb = b.spatulas;
      if (sb) sb = sb[0];
      if (!sa) sa = "noSpatula";
      if (!sb) sb = "noSpatula";
      if (sa === sb) {
        return a.totalCost - b.totalCost;
      }
      if (sa < sb) return -1;
      return 1;
    });
  // } else {
  //   teamComps.sort((a, b) => a.totalCost - b.totalCost);
  // }

  teamComps.forEach(teamComp => {
    rows.push(<Row key={teamComp.team.toString() + (teamComp.spatulas? teamComp.spatulas.toString() : "")} teamComp={teamComp} />);
  });
  const score = teamComps[0].score;

  let footer = null;
  if (wasFilterApplied) {
    footer = (<div className="sectionFooter">
      * {originaLength - numMarginallyBetter} other {teamComps[0].team.length}-champions comps not shown that are almost as synergistic as these.
    </div>)
  }
  return (
    <div className="section">
      <h1 className="sectionHeader">
      {teamSize}-Champions Teams ({rows.length}) - #active traits: {score}
      </h1>
      {rows}
      {footer}
    </div>
  );
}

export default Section;
