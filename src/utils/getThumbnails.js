// Write Javascript code here
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');


const URL = "https://tftactics.gg/champions";

const download = (uri, filename, callback) => {
    request.head(uri, function (err, res, body) {
        //console.log('content-type:', res.headers['content-type']);
        //console.log('content-length:', res.headers['content-length']);
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

const requestBody = (url, cb) => {
    request(url, (err, res, body) => {
        if (err) console.log(err);
        else cb(body);
    });
}

requestBody(URL, (body) => {
    let $ = cheerio.load(body);
    const champions = $('.character-icon');
    console.log(champions.length);
    console.log(champions.text());
    // .map(function (index) {
    //     console.log(index);
    //     const name = $(this).text();
    //     const thumbnailUrl = $(this).attr("src");
    //     console.log(thumbnailUrl);
    //     return { name, thumbnailUrl }
    // }).toArray();
    fs.writeFileSync('./championsThumbanails.json', JSON.stringify(champions, null, 2));
    champions.forEach(champion => {
        // download(champion.thumbnailUrl, './downloadedThumbnails/' + champion.name + '.png', () => {});
    })
});




