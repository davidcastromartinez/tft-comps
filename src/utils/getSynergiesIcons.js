// Write Javascript code here
const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');

const synergies = require('../datasets/synergies.json');

const tags = [...Object.keys(synergies.classes) , ...Object.keys(synergies.origins)];

const baseUrl = "https://rerollcdn.com/icons/";

const download = (uri, filename, callback = () => { }) => {
    request.head(uri, function (err, res, body) {
        //console.log('content-type:', res.headers['content-type']);
        //console.log('content-length:', res.headers['content-length']);
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};


tags.forEach(tag => {
    download(baseUrl + tag + '.png', './downloadedSynergiesThumbnails/' + tag + '.png');
});

