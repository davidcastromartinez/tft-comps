
const champions = require('../datasets/champions5_5.json');

let tags = champions.reduce((prev, curr, ind, arr) => {
    return  [...prev, ...curr.tags ];
}, []);

tags = [... new Set(tags)];

console.log(tags);

const synergies = {
    classes: {}
};

tags.forEach(tag => {
    synergies.classes[tag] = [];
})

console.log(JSON.stringify(synergies, null, 2));