const results = require('./datasets/results/currentResults.json');

const {data} = results;

let list = data[7];

list = list.sort((a, b) => {
  let sa = a.spatulas[0];
  let sb = b.spatulas[0];
  if (!sa) sa = "noSpatula";
  if (!sb) sb = "noSpatula";
  if (sa === sb) return 0;
  if (sa < sb) return -1;
  return 1;
});

console.log(list);
