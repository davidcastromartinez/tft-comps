const fs = require('fs');
const path = require('path');
const puppeteer = require('puppeteer');

const buildPng = (async (inputFilePath, suffix = "", vwWidth = null) => {
  const json = fs.readFileSync(inputFilePath);
  const jsonData = JSON.parse(json);
  console.log("Building image for name: " + jsonData.description.name);
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  page.on('console', msg => console.log('PAGE LOG:', msg.text()));
  await page.goto('http://localhost:3000/?q=' + Date.now());
  let width = 1650;
  if (vwWidth !== null) width = vwWidth;
  page.setViewport({ width, height: 1024 });

  const outputPath = 'outputs/output.png';
  const copyPath = 'outputs/' + jsonData.description.name + suffix + '.png';
  // await page.screenshot({path: 'outputs/output2.jpg', fullPage: true, type: "jpeg", quality: 99});
  await page.screenshot({path: outputPath, fullPage: true, type: "png"});
  console.log('Created ' + outputPath);
  fs.copyFile(outputPath, copyPath, (err) => {
    if (err) throw err;
    console.log('Created ' + copyPath);
  });
  await browser.close();
});

module.exports = buildPng;
